//
//  PremiumCell.swift
//  InAppPurchase
//
//  Created by ThangMacBookPro on 2/6/20.
//  Copyright © 2020 Đức Thắng MacBookPro. All rights reserved.
//

import UIKit

class PremiumCell: UITableViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var titleCell: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func bind(item:item){
        self.imageCell.image = item.image
        self.titleCell.text = item.title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
