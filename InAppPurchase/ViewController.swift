//
//  ViewController.swift
//  InAppPurchase
//
//  Created by ThangMacBookPro on 2/5/20.
//  Copyright © 2020 Đức Thắng MacBookPro. All rights reserved.
//

import UIKit
import FLAnimatedImage
import PromiseKit
import PMSuperButton

struct item {
    var title:String?
    var image:UIImage?
}

enum data {
    case item(item)
    case button(String)
}

class ViewController: UIViewController {
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    //MARK:- Properties
    var items:[item] = [.init(title: "don't limited features to use", image: #imageLiteral(resourceName: "baseline_lock_black_48pt")),
                        .init(title: "no any ads", image: #imageLiteral(resourceName: "baseline_close_black_48pt")),
                        .init(title: "support consumer", image: #imageLiteral(resourceName: "baseline_camera_enhance_black_48pt"))
    ]
    
    var dataButton = ["Week",
                      "Year"
    ]
    
    var datas:[data]?
    
    @IBOutlet weak var freeTrial: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subViewInScrollView: UIView!
    lazy var textForDiscount = UILabel()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        firstly {
            fetchGIFFromURL(url: URL.init(string: "https://media.giphy.com/media/xT9IgN8YKRhByRBzMI/giphy.gif")!)
        }.then { (data) in
            Promise<FLAnimatedImageView> { steal in
                let imageView = FLAnimatedImageView()
                imageView.contentMode = UIView.ContentMode.scaleAspectFit
                imageView.animatedImage = FLAnimatedImage(animatedGIFData: data)
                steal.fulfill(imageView)
            }
        }.done { (imageView) in
            
            self.freeTrial.apply{
                $0.layer.cornerRadius = 35
                UIView.animate(withDuration: 0.9, delay: 0.3,
                               usingSpringWithDamping: 0.25,
                               initialSpringVelocity: 0.0,
                               options: [.repeat, .autoreverse],
                               animations: {
                                self.freeTrial.layer.position.x += 10
                                self.freeTrial.layer.cornerRadius = 30
                                self.freeTrial.layer.transform = CATransform3DMakeScale(1.05, 1.05, 1.05)
                }){ _ in
                    self.freeTrial.layer.position.x -= 20
                    self.freeTrial.layer.cornerRadius = 35
                    self.freeTrial.layer.transform = CATransform3DMakeScale(1, 1, 1)
                }
            }
            
            self.textForDiscount.apply{
                $0.textColor = .white
                $0.translatesAutoresizingMaskIntoConstraints = false
                $0.text = "Save 80%"
                $0.layer.backgroundColor  = UIColor.red.cgColor
                $0.layer.cornerRadius = 10
                $0.textAlignment = .center
                $0.sizeToFit()
                self.freeTrial.addSubview($0)
                NSLayoutConstraint.activate([
                    self.freeTrial.topAnchor.constraint(equalTo: $0.topAnchor, constant: 10),
                    self.freeTrial.trailingAnchor.constraint(equalTo: $0.trailingAnchor, constant: 40),
                    $0.widthAnchor.constraint(equalToConstant: 100)
                ])
            }
            
            imageView.translatesAutoresizingMaskIntoConstraints =  false
            imageView.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
            self.subViewInScrollView.addSubview(imageView)
            self.subViewInScrollView.sendSubviewToBack(imageView)
            NSLayoutConstraint.activate([
                self.subViewInScrollView.centerXAnchor.constraint(equalTo: imageView.centerXAnchor, constant: 0),
                self.subViewInScrollView.centerYAnchor.constraint(equalTo: imageView.centerYAnchor, constant: 200)
            ])
        }.catch { (err) in
            print("ERROR >>>>>>>>",err)
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
       // self.tableHeight?.constant = tableView.contentSize.height
    }
    
    //MARK:- fetch Gif
    func fetchGIFFromURL(url:URL) -> Promise<Data> {
        return Promise { steal in
            do {
                steal.fulfill(try Data(contentsOf: url))
            }catch(let err){
                steal.reject(err)
            }
        }
    }
    
}
//MARK:- datasource for tableView
extension ViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataButton.count + items.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < items.count {
            return UIScreen.main.bounds.height/19
        }else{
            return 80
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < items.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PremiumCell", for: indexPath) as! PremiumCell
            cell.bind(item: items[indexPath.row])
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonPreniumCell", for: indexPath) as! ButtonPreniumCell
            cell.bind(title: dataButton[indexPath.row-items.count])
            return cell
        }
    }
}

import Foundation

protocol ScopeFunc { }

extension ScopeFunc {
    @inline(__always) func apply(_ block: (Self) -> ()) -> Self {
        block(self)
        return self
    }
    @discardableResult
    @inline(__always) func letIt<R>(_ block: (Self) -> R) -> R {
        return block(self)
    }
}

extension NSObject: ScopeFunc { }
