//
//  ButtonPreniumCell.swift
//  InAppPurchase
//
//  Created by ThangMacBookPro on 2/6/20.
//  Copyright © 2020 Đức Thắng MacBookPro. All rights reserved.
//

import UIKit
import PMSuperButton

class ButtonPreniumCell: UITableViewCell {

    @IBOutlet weak var buttons: PMSuperButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(title:String){
        self.buttons.setTitle(title, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
